        <div class="row">
          <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                <h5 class="card-title"><strong>Permintaan Pelayanan Hari Ini</strong></h5>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="progress-group row">
                      <div class="col-10">
                        <a href="#" class="btn btn-primary btn-block text-left btn-rounded pl-5">
                        Unit IT
                      </a>
                      </div>
                      <div class="col-2">
                        <a href="#" class="btn btn-primary btn-block btn-rounded">
                        15
                      </a>
                      </div>                      
                    </div>
                    <!-- /.progress-group -->

                    <div class="progress-group row">
                      <div class="col-10">
                        <a href="#" class="btn btn-danger btn-block text-left btn-rounded pl-5">
                        Unit IPS
                      </a>
                      </div>
                      <div class="col-2">
                        <a href="#" class="btn btn-danger btn-block btn-rounded">
                        15
                      </a>
                      </div>                      
                    </div>
                    <!-- /.progress-group -->

                    <div class="progress-group row">
                      <div class="col-10">
                        <a href="#" class="btn btn-success btn-block text-left btn-rounded pl-5">
                        Unit ITP
                      </a>
                      </div>
                      <div class="col-2">
                        <a href="#" class="btn btn-success btn-block btn-rounded">
                        15
                      </a>
                      </div>                      
                    </div>
                    <!-- /.progress-group -->

                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- ./card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->

          <div class="col-md-6">
            <div class="card">
              <div class="card-header">
                <h5 class="card-title"><strong>Penanganan Permintaan Hari Ini</strong></h5>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <div class="progress-group row">
                      <div class="col-2 text-right">
                        <a href="#" class="btn btn-primary btn-block btn-rounded">
                        15
                      </a>
                      </div> 
                      <div class="col-10">
                        <a href="#" class="btn btn-primary btn-block text-right btn-rounded pr-5">
                        Unit IT
                      </a>
                      </div>                     
                    </div>
                    <!-- /.progress-group -->

                    <div class="progress-group row">
                      <div class="col-2 text-right">
                        <a href="#" class="btn btn-danger btn-block btn-rounded">
                        15
                      </a>
                      </div>
                      <div class="col-10">
                        <a href="#" class="btn btn-danger btn-block text-right btn-rounded pr-5">
                        Unit IPS
                      </a>
                      </div>                      
                    </div>
                    <!-- /.progress-group -->

                    <div class="progress-group row">
                      <div class="col-2 text-right">
                        <a href="#" class="btn btn-success btn-block btn-rounded">
                        15
                      </a>
                      </div> 
                      <div class="col-10">
                        <a href="#" class="btn btn-success btn-block text-right btn-rounded pr-5">
                        Unit ITP
                      </a>
                      </div>                     
                    </div>

                    <!-- /.progress-group -->
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- ./card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header border-0">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">Laporan Aktifitas | Maret 2020</h3>
                  <a href="javascript:void(0);">Grafik | Tabel</a>
                </div>
              </div>
              <div class="card-body">
                <div class="d-flex">
                  <p class="d-flex flex-column">
                    <span class="text-bold text-lg">820</span>
                    <span>Visitors Over Time</span>
                  </p>
                  <p class="ml-auto d-flex flex-column text-right">
                    <span class="text-success">
                      <i class="fas fa-arrow-up"></i> 12.5%
                    </span>
                    <span class="text-muted">Since last week</span>
                  </p>
                </div>
                <!-- /.d-flex -->

                <div class="position-relative mb-4">
                  <div class="chartjs-size-monitor">
                    <div class="chartjs-size-monitor-expand">
                      <div class=""></div>
                    </div>
                    <div class="chartjs-size-monitor-shrink">
                      <div class=""></div>
                    </div>
                  </div>
                  <canvas id="visitors-chart" height="250" width="451" class="chartjs-render-monitor"
                    style="display: block; height: 200px; width: 361px;"></canvas>
                </div>

                <div class="d-flex flex-row justify-content-end">
                  <span class="mr-2">
                    <i class="fas fa-square text-primary"></i> This Week
                  </span>

                  <span>
                    <i class="fas fa-square text-gray"></i> Last Week
                  </span>
                </div>
              </div>
            </div>
            <!-- /.card -->
          </div>
        </div>