<p class="login-box-msg">Tuliskan detail akun Anda untuk masuk</p>
<?=$this->session->flashdata('name');?>
<?=validation_errors('<div class="alert alert-danger" role="alert">', '</div>');;?>
<form action="" method="post">
  <div class="input-group mb-3">
    <input type="text" class="form-control" name="username" placeholder="Username" autocomplete="off">
    <div class="input-group-append">
      <div class="input-group-text">
        <span class="fas fa-key"></span>
      </div>
    </div>
  </div>
  <div class="input-group mb-3">
    <input type="password" class="form-control" name="password" placeholder="Password" autocomplete="off">
    <div class="input-group-append">
      <div class="input-group-text">
        <span class="fas fa-lock"></span>
      </div>
    </div>
  </div>
  <p class="mb-3">
    <a href="<?=base_url('adminpusat/auth/lupa');?>">Lupa password</a>
  </p>
  <div class="row">
    <div class="col-12">
      <!-- <div class="icheck-primary">
        <input type="checkbox" id="remember">
        <label for="remember">
          Remember Me
        </label>
      </div> -->
      <button type="submit" class="btn btn-primary btn-block mt-4">Sign In</button>
    </div>
    <!-- /.col -->
  </div>
</form>