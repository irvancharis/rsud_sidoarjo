<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_unit extends CI_Model {

	public function data()
	{
		return $this->db->get('units')->result();
	}

	public function jumlah() // jumlah seluruh unit
	{
		return $this->db->get('units')->num_rows();
	}

	public function petugas()
	{
		# code...
	}

	public function perbaikan()
	{
		# code...
	}

	public function pending()
	{
		# code...
	}

}

/* End of file M_unit.php */
/* Location: ./application/modules/adminpusat/models/M_unit.php */