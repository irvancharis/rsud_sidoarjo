<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_auth extends CI_Model {

	public function login()
	{
		$data = $this->input->post();
		$this->db->select('a.unit_id, a.role_id, a.useraccount_id as id, c.name as nama');
		$this->db->join('user_accounts c', 'c.id = a.useraccount_id');
		$this->db->where('a.username', $data['username']);
		$this->db->where('a.password', md5($data['password']));
		$this->db->where('a.role_id', '1'); // 1 untuk Admin Pusat
		return $this->db->get('user_admin a')->row();
	}

}

/* End of file M_auth.php */
/* Location: ./application/modules/adminpusat/models/M_auth.php */