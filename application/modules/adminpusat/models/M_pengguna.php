<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pengguna extends CI_Model {
	
	public function id()
	{
		$data = $this->db->select('id+1 as id')
						 ->from('user_accounts')
						 ->order_by('id','desc')
						 ->get()
						 ->row();
		return $data->id;
	}

	public function data($status='1')
	{
		$unit_id = $this->input->post('unit_id') ?? 1;

		return $this->db->select('c.name, c.id')
				 ->from('user_admin a')
				 ->join('user_accounts c','c.id=a.useraccount_id')
				 ->join('roles r','r.id=a.role_id')
				 ->where('c.status',$status)
				 ->where('a.role_id',$unit_id)
				 ->get()
				 ->result();
	}

	public function tambah() 
	{
		$post = $this->input->post();
		$id = $this->id(); // id user_accounts

		// input ke user_admin
		$data = [
			'useraccount_id' => htmlspecialchars(trim($id)),
			'role_id' => htmlspecialchars(trim($post['role_id'])),
			'unit_id' => htmlspecialchars(trim($post['unit_id'])),
			'username' => htmlspecialchars(trim($post['username'])),
			'password' => htmlspecialchars(trim($post['password'])),
			'user_ent' => $this->session->id
		];
		$this->db->set('date_ent','now()',false);
		$cek = $this->db->insert('user_admin', $data);

		if ($cek) {
			// input ke user_accounts
			$data = [
				'unit_id' => htmlspecialchars(trim($post['unit_id'])),
				'name' => htmlspecialchars(trim($post['name'])),
				'email' => htmlspecialchars(trim($post['email'])),
				'phone' => htmlspecialchars(trim($post['phone'])),
				'status' => htmlspecialchars(trim($post['status'])),
				'user_ent' => $this->session->id
			];
			$this->db->set('date_ent','now()',false);
			$cek = $this->db->insert('user_accounts', $data);
			if ($cek) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}	
	}

	public function ubah()
	{
		$post = $this->input->post();
		$id = htmlspecialchars(trim($post['id']));

		// input ke user_admin
		$data = [
			'role_id' => htmlspecialchars(trim($post['role_id'])),
			'unit_id' => htmlspecialchars(trim($post['unit_id'])),
			'username' => htmlspecialchars(trim($post['username'])),
			'password' => htmlspecialchars(trim($post['password'])),
			'user_ent' => $this->session->id
		];
		$this->db->set('date_ent','now()',false);
		$where = ['useraccount_id', $id];
		$cek = $this->db->update('user_admin', $data, $where);

		if ($cek) {
			// input ke user_accounts
			$data = [
				'unit_id' => htmlspecialchars(trim($post['unit_id'])),
				'name' => htmlspecialchars(trim($post['name'])),
				'email' => htmlspecialchars(trim($post['email'])),
				'phone' => htmlspecialchars(trim($post['phone'])),
				'status' => htmlspecialchars(trim($post['status'])),
				'user_ent' => $this->session->id
			];
			$this->db->set('date_ent','now()',false);
			$where = ['id', $id];
			$cek = $this->db->update('user_accounts', $data, $where);
			if ($cek) {
				return true;
			} else {
				return false;
			}	
		} else {
			return false;
		}	
	}

	public function hapus()
	{
		$post = $this->input->post();
		$id = htmlspecialchars(trim($post['id']));

		// hapus user_accounts
		$this->db->delete('user_accounts', compact('id'));
		return $this->db->delete('user_admin', ['useraccount_id'=>$id]);
	}

	public function jumlah($status='1')
	{
		$data = $this->db->select('a.id')
				 ->from('user_admin a')
				 ->join('user_accounts c','c.id=a.useraccount_id')
				 ->join('roles r','r.id=a.role_id')
				 ->where('c.status',$status)
				 ->get();
		return $data->num_rows();
	}

}

/* End of file M_pengguna.php */
/* Location: ./application/modules/adminpusat/models/M_pengguna.php */