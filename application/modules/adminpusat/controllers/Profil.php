<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		parent::auth('adminpusat');
		$this->load->model('adminpusat/M_profil','mpro');
	}

	public function index()
	{
		// $data = $this->mpro->detail($this->session->id);
		// $this->load->view('profil/detail', $data);
		$data = [
			'title' => 'Akun Saya',
			'hal' => 'profil/detail'
		];

		$this->load->view('layout', $data);
	}

	public function edit()
	{
		$this->load->library('form_validation');

		if ($this->form_validation->run() == FALSE) {
			$data = $this->mpro->detail($this->session->id);
			$this->load->view('profil/edit', $data);
		} else {
			$id = $this->session->id ?? redirect('adminpusat/auth','refresh');
			$cek = $this->mpro->ubah();
			if ($cek) {
				return true;
			} else {
				return false;
			}
		}
	}

}

/* End of file Profil.php */
/* Location: ./application/modules/adminpusat/controllers/Profil.php */