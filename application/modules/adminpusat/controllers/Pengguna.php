<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		parent::auth('adminpusat');
		$this->load->model('adminpusat/M_pengguna','mp');
	}

	public function index()
	{
		// $data = [
		// 	'data' => $this->mp->data(),
		// 	'jumlah_pengguna' => $this->mp->jumlah()
		// ];
		// $this->load->view('pengguna/index', $data);

		$data = [
			'title' => 'Kelola Data Pengguna',
			'hal' => 'pengguna/index'
		];
		$this->load->view('layout', $data);
	}

	public function tambah()
	{
		$this->load->library('form_validation');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('profil/edit');
		} else {
			$cek = $this->simpan();
			if ($cek) {
				return true;
			} else {
				return false;
			}
		}
	}

	public function simpan()
	{
		$this->load->library('form_validation');

		if ($this->form_validation->run() == TRUE) {
			return true;
		} else {
			return false;
		}
	}

	public function edit()
	{
		$this->load->library('form_validation');

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('profil/edit');
		} else {
			$cek = $this->simpan();
			if ($cek) {
				return true;
			} else {
				return false;
			}
		}
	}

	public function hapus()
	{
		$cek = $this->mp->hapus();
		if ($cek) {
			return true;
		} else {
			return false;
		}
		
	}

}

/* End of file Pengguna.php */
/* Location: ./application/modules/adminpusat/controllers/Pengguna.php */