<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_auth','ma');

	}

	public function index()
	{
		parent::login('adminpusat');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => 'Masuk ke Dashboard Admin Pusat',
				'hal' => 'auth/index'
			];
			$this->load->view('auth', $data);
		} else {
			$this->_login();
		}
	}

	private function _login()
	{
		$cek = $this->ma->login();
		if ($cek) {
			$data = [
				'id' => $cek->id,
				'nama' => $cek->nama,
				'role_id' => $cek->role_id,
				'unit_id' => $cek->unit_id,
			];
			$this->session->set_userdata($data);
			$this->session->set_flashdata('name', '<div class="alert alert-success" role="alert">Selamat datang kembali!</div>');
			redirect('adminpusat/dashboard/board','refresh');
		} else {
			$this->session->set_flashdata('name', '<div class="alert alert-danger" role="alert">Gagal masuk sistem!</div>');
			redirect('adminpusat/auth','refresh');
		}
		
	}

	public function lupa()
	{
		parent::login('adminpusat');
		$data = [
			'title' => 'Lupa Password Admin Pusat',
			'js' => 'auth/lupa_js',
			'css' => 'auth/lupa_css',
			'hal' => 'auth/lupa'
		];
		$this->load->view('auth', $data);
	}

	public function recover()
	{
		parent::login('adminpusat');
		$data = [
			'title' => 'Masuk ke Dashboard Admin',
			'js' => 'auth/recover_js',
			'css' => 'auth/lupa_css',
			'hal' => 'auth/recover'
		];
		$this->load->view('auth', $data);
	}

	public function dilarang()
	{
		echo "<h1>Dilarang</h1>";
	}

	public function keluar()
	{
		unset($_SESSION['id']);
		unset($_SESSION['nama']);
		unset($_SESSION['role_id']);
		unset($_SESSION['unit_id']);
		session_destroy();
		redirect('adminpusat/auth','refresh');
	}

}

/* End of file Auth.php */
/* Location: ./application/modules/adminpusat/controllers/Auth.php */