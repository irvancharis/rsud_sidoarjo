<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
		parent::auth('adminpusat');
	}

	public function index()
	{
		$data = [
			'title' => 'Aktifitas Unit',
			'hal' => 'unit/index'
		];
		$this->load->view('layout', $data);
	}

	public function laporan()
	{
		$data = [
			'title' => 'Laporan Aktifitas',
			'hal' => 'unit/index'
		];
		$this->load->view('layout', $data);
	}
}

/* End of file Unit.php */
/* Location: ./application/modules/adminpusat/controllers/Unit.php */