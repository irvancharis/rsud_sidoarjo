<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		parent::auth('adminpusat');
		//Do your magic here
	}

	public function index()
	{
		$this->load->model('adminpusat/M_pengguna','mpengguna');
		$this->load->model('adminpusat/M_unit','munit');
		$data = [
			'title' => 'Dashboard Admin Pusat',
			'hal' => 'dashboard/index',
			'data' => $this->mpengguna->data(),
			'total_pengguna' => $this->mpengguna->jumlah(),
			'total_unit' => $this->munit->jumlah()
		];
		$this->load->view('layout', $data);
	}

	public function board()
	{
		$this->load->view('dashboard/onboard');
	}

}

/* End of file Dashboard.php */
/* Location: ./application/modules/adminpusat/controllers/Dashboard.php */